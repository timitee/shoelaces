![](./timicard.png)

# shoelaces

## Installation

```
sudo apt install nodejs
sudo apt install npm
sudo apt install gulp

cd shoelaces
npm install
```

## Usage

### Start fresh with a new Bootstrap4 theme

```
# Copy the current version of the bootstrap variables file.
cp node_modules/bootstrap/scss/_variables.scss scss/_themables.scss

# Copy the current version of the bootstrap file.
cp node_modules/bootstrap/scss/bootstrap.scss scss/bootstrap.scss

# Create a place for custom themes. As a starter one file will be enough.
touch _theme.scc
```

### Edit scss/bootstrap.scss

Change:

```
@import "functions";
@import "variables";
```

To:

```
@import "functions";
@import "themables";
```

Add to the end of the file any custom SASS files. e.g.:

```
@import "theme";
```

Comment out any Bootstrap4 modules you don't want, e.g.:


```
// @import "pagination";
// @import "badge";
// @import "jumbotron";
```

### Edit gulpfile.js

Comment out any Bootstrap4 javascript modules you don't want, e.g.:

```
// // Bootstrap
// 'node_modules/bootstrap/js/src/alert.js',
// 'node_modules/bootstrap/js/src/button.js',
// 'node_modules/bootstrap/js/src/carousel.js',
```

Add references to any other resources from node_modules you want to package
up, e.g.:

```
var scss = [
    ...,
    'node_modules/example_app/dist/example_app.min.css',
    ...,
  ];

var scripts = [
  ...,
  'node_modules/example_app/dist/example_app.min.js',
  ...,
]
```

Change the paths where you want the minified/uglified static files built, e.g.:

```
gulp.task('sass', function() {
  ...
  pipe(gulp.dest('../path/to/my_app/static/css'))
```


![](./appicon.png)