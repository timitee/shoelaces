var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
var sass = require('gulp-sass');
var minify = require('gulp-minify-css');
var uglify = require('gulp-uglify');
// var gutil = require('gulp-util');
var browserSync = require('browser-sync').create();

var scss = [
    'scss/*.scss',
  ];

var scripts = [
    // Jquery
    // 'node_modules/jquery/dist/jquery.min.js',
    // Bootstrap
    // 'node_modules/bootstrap/js/src/alert.js',
    // 'node_modules/bootstrap/js/src/button.js',
    // 'node_modules/bootstrap/js/src/carousel.js',
    // 'node_modules/bootstrap/js/src/collapse.js',
    // 'node_modules/bootstrap/js/src/dropdown.js',
    // 'node_modules/bootstrap/js/src/modal.js',
    // 'node_modules/bootstrap/js/src/popover.js',
    // 'node_modules/bootstrap/js/src/scrollspy.js',
    // 'node_modules/bootstrap/js/src/tab.js',
    // 'node_modules/bootstrap/js/src/tooltip.js',
    // 'node_modules/bootstrap/js/src/util.js',
    // 'node_modules/tether/dist/js/tether.min.js',
    // Timitools
    'js_library/timitools.js',
  ];

var html = [
  'html/*.html',
];

var watch = [
 'js_library/ckeditor_config_full.js',
].concat(scss,scripts,html);
// gutil.log(watch);

gulp.task('uglify', function() {
  return gulp.src(scripts)
    .pipe(concat('concat.js'))
    .pipe(gulp.dest('js'))
    .pipe(rename('uglify.js'))
    .pipe(uglify())
    .pipe(gulp.dest('js'))
    .pipe(rename('main.min.js'))
    .pipe(gulp.dest('../moretosay/static/js'))
});

// Compile sass into CSS & auto-inject into browsers
gulp.task('sass', function() {
  return gulp.src(scss)
    .pipe(sass({includePaths: ['node_modules/bootstrap/scss']}))
    .pipe(concat('concat.css'))
    .pipe(rename('theme.css'))
    .pipe(gulp.dest("css"))
    .pipe(rename('theme.min.css'))
    .pipe(minify())
    .pipe(gulp.dest('../moretosay/static/css'))
    .pipe(browserSync.stream())
});


// Minify css into CSS & auto-inject into browsers
gulp.task('minify', function() {
  return gulp.src(['css/*.css'])
    .pipe(concat('theme.min.css'))
    .pipe(minify())
    .pipe(gulp.dest('../moretosay/static/css'))
    .pipe(browserSync.stream())
});


// Static Server + watching scss/html files
gulp.task('watch', ['sass', 'minify'], function() {

  browserSync.init({server: "./"});

  gulp.watch(watch, ['uglify', 'sass', 'minify']);
  gulp.watch(watch).on('change', browserSync.reload);
});

gulp.task('default', ['uglify', 'watch']);
