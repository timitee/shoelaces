/*
http://bdadam.com/blog/simple-usability-trick-for-google-maps.html
### USAGE ###
1) Assign a div area the class name: no_scroll_until_click_map.
2) Ensure this script is bundled into the page.
2) Call the function from the page's "ready" function with parameters:
<script>
  $(document).ready(function () {
    var lat_lng = {lat: 50.741398, lng: -3.9806000};
    var zoom_level = 13; // where 1 = world && 17 = building
    var marker_label = 'Tim Bushell';
    noGoogleMapScrollUntilClick(lat_lng, zoom_level, marker_label);
  });
</script>

// check for touch screen
var isDraggable = !('ontouchstart' in document.documentElement);
var mapOptions = {
  draggable: isDraggable,
  scrollwheel: false
};
*/

function noGoogleMapScrollUntilClick(fn_lat_lng, fn_zoom_level, fn_marker_label) {
  if (window.google && window.google.maps) {
    window.loadMap(fn_lat_lng, fn_zoom_level, fn_marker_label);
    return;
  }
}

window.loadMap = function(fn_lat_lng, fn_zoom_level, fn_marker_label) {
  if (!window.$ || !window.google || !window.google) {
    return setTimeout(loadMap, 100);
  }

  $(function() {

    var map;
    var map_parent = $('#google-map');
    var map_latlng = new google.maps.LatLng(fn_lat_lng['lat'], fn_lat_lng['lng']);

    function init() {

      map = new google.maps.Map(map_parent[0], {
        zoom: fn_zoom_level,
        scrollwheel: false,
        draggable: !('ontouchstart' in document.documentElement),
        center: map_latlng,
        styles: [{
          featureType: 'poi',
          stylers: [{
            visibility: "off"
          }]
        }, ],
      });


      var marker = new google.maps.Marker({
        position: map_latlng,
        map: map,
        title: fn_marker_label,
        label: fn_marker_label[0],
      });

      custom_label = '<div>' + fn_marker_label + '</div>';
      txt = new TxtOverlay(map_latlng, custom_label, "google-text", map)

      // Create the DIV to hold the control and call the CenterControl()
      // constructor passing in this DIV.
      var centerControlDiv = document.createElement('div');
      var centerControl = new CenterControl(centerControlDiv, map);

      centerControlDiv.index = 1;
      map.controls[google.maps.ControlPosition.BOTTOM_CENTER].push(centerControlDiv);

      google.maps.event.addListener(map, 'mousedown', function() {
        enableScrollingWithMouseWheel()
      });
      google.maps.event.addListener(map, 'click', function() {
        enableDraggingOnPhone();
      });
    }

    google.maps.event.addDomListener(window, 'load', init);

    $('body').on('mousedown', function(event) {
      var clickedInsideMap = $(event.target).parents('#google-map').length > 0;
      if (!clickedInsideMap) {
        disableScrollingWithMouseWheel();
      }
    });

    $(window).scroll(function() {
      disableScrollingWithMouseWheel();
      disableDraggingOnPhone();
    });

    function enableDraggingOnPhone() {
      map.setOptions({
        draggable: true
      });
    }

    function disableDraggingOnPhone() {
      map.setOptions({
        draggable: !('ontouchstart' in document.documentElement)
      });
    }

    function enableScrollingWithMouseWheel() {
      map.setOptions({
        scrollwheel: true
      });
    }

    function disableScrollingWithMouseWheel() {
      map.setOptions({
        scrollwheel: false
      });
    }

    function CenterControl(controlDiv, map) {
      // Set CSS for the control border.
      var controlUI = ControlUI();
      controlUI.title = 'Click to recenter the map';
      controlDiv.appendChild(controlUI);
      // Set CSS for the control interior.
      var controlText = ControlText();
      controlText.innerHTML = 'Center Map';
      controlUI.appendChild(controlText);
      // Setup the click event listeners: simply set the map to Chicago.
      controlUI.addEventListener('click', function() {
        map.setCenter(map_latlng);
      });
    }



    // MAP
    function ControlUI() {
      var controlUI = document.createElement('div');
      controlUI.style.backgroundColor = '#fff';
      controlUI.style.border = '2px solid #fff';
      controlUI.style.borderRadius = '3px';
      controlUI.style.boxShadow = '0 2px 6px rgba(0,0,0,.3)';
      controlUI.style.cursor = 'pointer';
      controlUI.style.padding = '0';
      controlUI.style.marginBottom = '18px';
      controlUI.style.textAlign = 'center';
      controlUI.title = 'Click to recenter the map';
      return controlUI;
    }

    function ControlText() {
      var controlText = document.createElement('div');
      controlText.style.color = 'rgb(25,25,25)';
      controlText.style.fontFamily = 'Roboto,';
      controlText.style.fontSize = '12px';
      controlText.style.lineHeight = '28px';
      controlText.style.paddingLeft = '5px';
      controlText.style.paddingRight = '5px';
      return controlText;
    }

    //adapded from this example http://code.google.com/apis/maps/documentation/javascript/overlays.html#CustomOverlays
    //text overlays
    function TxtOverlay(pos, txt, cls, map) {
      // Now initialize all properties.
      this.pos = pos;
      this.txt_ = txt;
      this.cls_ = cls;
      this.map_ = map;
      // We define a property to hold the image's
      // div. We'll actually create this div
      // upon receipt of the add() method so we'll
      // leave it null for now.
      this.div_ = null;
      // Explicitly call setMap() on this overlay
      this.setMap(map);
    }
    TxtOverlay.prototype = new google.maps.OverlayView();
    TxtOverlay.prototype.onAdd = function() {
      // Note: an overlay's receipt of onAdd() indicates that
      // the map's panes are now available for attaching
      // the overlay to the map via the DOM.
      // Create the DIV and set some basic attributes.
      var div = document.createElement('DIV');
      div.className = this.cls_;
      div.innerHTML = this.txt_;
      // Set the overlay's div_ property to this DIV
      this.div_ = div;
      var overlayProjection = this.getProjection();
      var position = overlayProjection.fromLatLngToDivPixel(this.pos);
      div.style.left = position.x + 'px';
      div.style.top = position.y + 'px';
      // We add an overlay to a map via one of the map's panes.

      var panes = this.getPanes();
      panes.floatPane.appendChild(div);
    }
    TxtOverlay.prototype.draw = function() {
        var overlayProjection = this.getProjection();
        // Retrieve the southwest and northeast coordinates of this overlay
        // in latlngs and convert them to pixels coordinates.
        // We'll use these coordinates to resize the DIV.
        var position = overlayProjection.fromLatLngToDivPixel(this.pos);
        var div = this.div_;
        div.style.left = position.x + 'px';
        div.style.top = position.y + 'px';
      }
      //Optional: helper methods for removing and toggling the text overlay.
    TxtOverlay.prototype.onRemove = function() {
      this.div_.parentNode.removeChild(this.div_);
      this.div_ = null;
    }
    TxtOverlay.prototype.hide = function() {
      if (this.div_) {
        this.div_.style.visibility = "hidden";
      }
    }
    TxtOverlay.prototype.show = function() {
      if (this.div_) {
        this.div_.style.visibility = "visible";
      }
    }
    TxtOverlay.prototype.toggle = function() {
      if (this.div_) {
        if (this.div_.style.visibility == "hidden") {
          this.show();
        } else {
          this.hide();
        }
      }
    }
    TxtOverlay.prototype.toggleDOM = function() {
      if (this.getMap()) {
        this.setMap(null);
      } else {
        this.setMap(this.map_);
      }
    }
  });
};
